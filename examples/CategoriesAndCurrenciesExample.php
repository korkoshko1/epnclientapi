<?php

require_once __DIR__ . '/../vendor/autoload.php';

use korkoshko\EpnClientApi\EpnClientApi;
use korkoshko\EpnClientApi\Exceptions\EpnClientException;

$client = new EpnClientApi('', '');

try {
    $request = $client->currencies('cu_l')->categories('ca_l')->params(['lang' => 'ru']);

    var_dump(
        $request->get()
    );
} catch (EpnClientException $e) {
    echo $e->getMessage();
}
