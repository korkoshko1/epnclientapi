<?php

require_once __DIR__ . '/../vendor/autoload.php';

use korkoshko\EpnClientApi\EpnClientApi;
use korkoshko\EpnClientApi\Exceptions\EpnClientException;

$client = new EpnClientApi('', '');

$search = [
    'limit'     => 10,
    'price_max' => '1000',
    'currency'  => 'UAH',
    'lang'      => 'RU',
];

try {
    var_dump(
        $client->search('s_o')->params($search)->get()
    );

} catch (EpnClientException $e) {
    echo $e->getMessage();
}
