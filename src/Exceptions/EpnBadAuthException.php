<?php

namespace korkoshko\EpnClientApi\Exceptions;

/**
 * Class EpnBadAuthException
 *
 * @package korkoshko\EpnClientApi\Exceptions
 */
class EpnBadAuthException extends \Exception
{
    //
}