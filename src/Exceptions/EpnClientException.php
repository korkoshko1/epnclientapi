<?php

namespace korkoshko\EpnClientApi\Exceptions;

/**
 * Class EpnClientException
 *
 * @package korkoshko\EpnClientApi\Exceptions
 */
class EpnClientException extends \Exception
{
    //
}