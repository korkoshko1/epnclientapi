<?php

namespace korkoshko\EpnClientApi;

/**
 * Class RequestMethod
 * @package korkoshko\EpnClientApi
 */
class RequestMethod
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $params;

    /**
     * RequestMethod constructor.
     *
     * @param $name
     * @param $params
     */
    public function __construct(string $name, array $params)
    {
        $this->name   = $name;
        $this->params = $params;
    }

    /**
     * Set request name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add new params for request
     *
     * @param array $params
     *
     * @return $this
     */
    public function addParams(array $params)
    {
        $this->params = array_merge($this->params, $params);

        return $this;
    }

    /**
     *  Get request name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get request params
     *
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }
}