<?php

namespace korkoshko\EpnClientApi\Traits;

use korkoshko\EpnClientApi\Exceptions\EpnClientException;

/**
 * Trait Methods
 *
 * @package korkoshko\EpnClientApi\Traits
 */
trait Methods
{
    /**
     * @var array
     */
    private $methods = [
        'categories'         => 'list_categories',
        'currencies'         => 'list_currencies',
        'countForCategories' => 'count_for_categories',
        'offer'              => 'offer_info',
        'search'             => 'search',
        'topMonthly'         => 'top_monthly',
    ];

    /**
     * @param $name
     * @param $arguments
     *
     * @return $this
     * @throws EpnClientException
     */
    public function __call($name, $args)
    {
        if (!key_exists($name, $this->methods)) {
            throw new EpnClientException("Method {$name} is not supported'");
        }

        $this->callMethod($name, $args);

        return $this;
    }

    /**
     * @param $name
     * @param $args
     * @return $this
     */

    protected function callMethod($name, $args)
    {
        return $this;
    }
}