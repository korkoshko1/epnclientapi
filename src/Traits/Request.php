<?php

namespace korkoshko\EpnClientApi\Traits;

use korkoshko\EpnClientApi\Exceptions\{
    EpnClientException,
    EpnBadAuthException
};

/**
 * Trait Request
 *
 * @package korkoshko\EpnClientApi\Traits
 */
trait Request
{
    /**
     * Errors from api
     *
     * @var array
     */
    protected $exceptions = [
        'namespace'      => 'korkoshko\EpnClientApi\Exceptions\\',
        '*'              => 'EpnClientException',
        'Bad auth data!' => 'EpnBadAuthException',
    ];

    /** Send post request
     *
     * @param string $url
     * @param array  $params
     *
     * @return array
     * @throws EpnClientException
     */
    protected function request(string $url, array $params): array
    {
        $ch = curl_init($url);

        $params = json_encode($params);

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $params,
        ]);

        $response = $this->getResponse($ch);

        curl_close($ch);

        return $response;
    }

    /**
     * Get response to the request
     *
     * @param $ch
     *
     * @return array
     * @throws EpnClientException|EpnBadAuthException
     */
    protected function getResponse($ch): array
    {
        $response = curl_exec($ch);

        if (!$response) {
            throw new EpnClientException('Request failed: Error ' . curl_errno($ch));
        }

        $response = json_decode($response, true);

        if (!empty($response['error'])) {
            $error = $response['error'];
            $errorMessage = 'API Error: ' . $error;

            $this->throwException($error, $errorMessage);
        }

        return $response['results'];
    }

    /**
     * Get response to the request
     *
     * @param $error string
     * @param $errorMessage string
     *
     * @return object
     */
    private function throwException($error, $errorMessage)
    {
        if (isset($this->exceptions[$error])) {
            $exception = $this->exceptions['namespace'] . $this->exceptions[$error];

            throw new $exception($errorMessage);
        }

        $exception = $this->exceptions['namespace'] . $this->exceptions['*'];

        throw new $exception($errorMessage);
    }
}